package obserwator;

public class SoundAlarm {
	public void alarmTurnedOn(EnteredPinEvent event) {
        System.out.println("SOUND ALARM UNLOCKED");
    }
    
    public void alarmTurnedOff(EnteredPinEvent event) {
        System.out.println("SOUND ALARM LOCKED");
    }
}
