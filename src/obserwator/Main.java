package obserwator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Alarm alarm = new Alarm("123");
		
		alarm.check("321");
		alarm.addListener(Dogs());
		alarm.addListener(Police());
		alarm.addListener(SoundAlarm());
		
		alarm.check("123");
		alarm.removeListener(Dogs());
		alarm.addListener(Police());
		alarm.addListener(SoundAlarm());
	}

}
