package obserwator;

public class Police {
	public void alarmTurnedOn(EnteredPinEvent event) {
        System.out.println("POLICE CALLED");
    }
    
    public void alarmTurnedOff(EnteredPinEvent event) {
        System.out.println("POLICE CALL DENIED");
    }
}
