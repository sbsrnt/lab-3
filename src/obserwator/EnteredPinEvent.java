package obserwator;

public class EnteredPinEvent {
	public Alarm alarm;
	
	public EnteredPinEvent(Alarm alarm)
	{
		this.alarm= alarm;
	}
	
	public Alarm getAlarm()
	{
		return alarm;
	}
}
