package obserwator;

import java.util.ArrayList;
import java.util.List;


public class Alarm{
	
	public String pin;
	private List<AlarmListener> listeners;

	public Alarm(){
		this.pin ="123";
	}
	
	public Alarm(String pin)
	{
		this.pin=pin;
		this.listeners = new ArrayList<AlarmListener>();		
	}
	
	public void addListener(AlarmListener listener)
	{
		this.listeners.add(listener);
	}
	
	public void removeListener(AlarmListener listener)
	{
		this.listeners.remove(listener);
	}
	
	public void goodPin(){
		AlarmListener.alarmTurnedOff(new EnteredPinEvent);
	}
	public void badPin(){
		AlarmListener.alarmTurnedOn(new EnteredPinEvent);
	}
	
	public void check(String pin)
	{
		if(this.pin == pin){
			goodPin();
		}
		else badPin();
	}
	
	
}